# MongoDB - Concert Management



## First things first

This project is the result of a MongoDB course I took during my associate's degree.
Thanks to this experience I learnt:
- how document data structures work and how to implement a programming logic on its characteristics
- MongoDB documentation
- DB communication between Python and MongoDB
- Server as a service with MongoDB Compass
- geoquery on MongoDB

## Description

This project needs to able to manage the purchase of concerts' tickets.
the key requirements are:
- the user can filter all the concerts thanks to their location, time range, artists playing and concert name
- geo query a concert location and see the available concerts within a certain radius
- an user must be able to see how many tickets are still available
- an user must be able to buy more than one ticket at a time
- an user cannot buy a ticket for a sold-out event
- all the updates must be saved in MongoDB Compass, so that next time someone starts the program, all the data are updated to the lastest version
